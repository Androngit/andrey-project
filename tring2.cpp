#include<iostream>
#include<math.h>
#include<string>
int main() {
	std::cout << "Enter number" << std::endl;
	float b;
	std::cin >> b;
	for (int i = 0; i < b; ++i) {
		for (int j = 0; j < b; ++j) {
			if (i == 0 || j >= i )
				std::cout << "*";
			else
				std::cout << " ";
		}
		std::cout << std::endl;
	}
	return 0;
}