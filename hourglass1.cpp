#include<iostream>
#include<math.h>
#include<string>
int main() {
	std::cout << "Enter number" << std::endl;
	float b;
	std::cin >> b;
	for (int i = 0; i < b; ++i) {
		for (int j = 0; j < b; ++j) {
			if ((i < b/2 && j >= i && j <= b - i- 1) || (i >= b/2 && j <= i && j >= b - i -1))
				std::cout << "*";
			else
				std::cout << " ";
		}
		std::cout << std::endl;
	}
	return 0;
}